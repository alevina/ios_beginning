//
//  AlarmTableViewController.swift
//  Storyboard
//
//  Created by alevina on 7/10/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import UIKit

class AlarmTableViewController: UITableViewController {
    
    //MARK: Properties
    var alarms = [Alarm] ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set view background color
        view.backgroundColor =
                    UIColor(red: 37/255.0, green: 50/255.0, blue: 56/255.0,
                    alpha: 1.0)

        // Create custom navigation bar buttons
        navigationItem.leftBarButtonItem =
                    createBarButton(image: "MenuButton.png", width: 30,
                    height: 30)
        navigationItem.rightBarButtonItems![1] =
                    createBarButton(image: "SettingsButton.png", width: 35,
                    height: 30)
        
        // Load the sample data
        loadSampleAlarms()
        
        // Create bottomViews
        createBottomView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView,
                numberOfRowsInSection section: Int) -> Int {
        return alarms.count
    }
    
    override func tableView(_ tableView: UITableView,
                cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "AlarmTableViewCell"
        guard let cell =
                    tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                    for: indexPath) as? AlarmTableViewCell else {
            fatalError("The dequeued cell is not an instance of "
                + "AlarmTableViewCell.")
        }
        
        let alarm = alarms[indexPath.row]
        
        cell.alarmButton.setTitle(alarm.time, for: .normal)
        cell.awakeChecker.text = alarm.awakeChecker
        cell.enableAlarmSwitch.isOn = alarm.isEnabled
        cell.periodicity.text = alarm.periodicity
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView,
                viewForHeaderInSection section: Int) -> UIView? {
        let frame = tableView.frame
        let headerView = UIView(frame: CGRect.init(x: 0, y: 0,
                    width: frame.size.width, height: frame.size.height))
        
        let title = UILabel()
        title.translatesAutoresizingMaskIntoConstraints = false
        title.font = UIFont.boldSystemFont(ofSize: 13.0)
        title.text = "Отключено"
        title.textColor = UIColor(red: 106/255.0, green: 115/255.0,
                    blue: 118/255.0, alpha: 1.0)
        headerView.addSubview(title)
        
        let removeButton = UIButton.init(type: .custom)
        removeButton.translatesAutoresizingMaskIntoConstraints = false
        removeButton.tag = section
        removeButton.isEnabled = true
        let removeButtonImage = UIImage.init(named: "RemoveButton.png")
        removeButton.setImage(removeButtonImage, for: UIControlState.normal)
        headerView.addSubview(removeButton)
        
        var viewsDict = Dictionary <String, UIView>()
        viewsDict["title"] = title
        viewsDict["removeButton"] = removeButton
        
        headerView.addConstraints(NSLayoutConstraint.constraints(
                    withVisualFormat: "H:|-12-[title]-[removeButton]-18-|",
                    options: NSLayoutFormatOptions(rawValue: 0), metrics: nil,
                    views: viewsDict))
        
        headerView.addConstraints(NSLayoutConstraint.constraints(
                    withVisualFormat: "V:|-[title]-|",
                    options: NSLayoutFormatOptions(rawValue: 0), metrics: nil,
                    views: viewsDict))
        
        headerView.addConstraints(NSLayoutConstraint.constraints(
                    withVisualFormat: "V:|-[removeButton]-|",
                    options: NSLayoutFormatOptions(rawValue: 0), metrics: nil,
                    views: viewsDict))

        return headerView
    }
    
    override func tableView(_ tableView: UITableView,
                heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView,
                heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    //MARK: Private methods
    private func createBarButton(image: String, width: Int, height: Int)
                -> UIBarButtonItem {
        let button = UIButton.init(type: .custom)
        
        button.setImage(UIImage.init(named: image),
                    for: UIControlState.normal)
        button.frame = CGRect.init(x: 0, y: 0, width: width, height: height)
        
        let barButton = UIBarButtonItem.init(customView: button)
        return barButton
    }
    
    private func createBottomView() {
        let margins = navigationController!.view.layoutMarginsGuide
        
        let ad = addAdvertisement()
        let purchase = addPurchasing()
        let android = addAndroid()
        let moonButton = addMoonButton()
        
        ad.bottomAnchor.constraint(equalTo: margins.bottomAnchor,
                    constant: 0).isActive = true
        purchase.bottomAnchor.constraint(equalTo: ad.topAnchor,
                    constant: 0).isActive = true
        android.bottomAnchor.constraint(equalTo: purchase.topAnchor,
                    constant: 0).isActive = true
        moonButton.trailingAnchor.constraint(equalTo: margins.trailingAnchor,
                    constant: 4).isActive = true
        moonButton.bottomAnchor.constraint(equalTo: purchase.topAnchor,
                    constant: -12).isActive = true
    }
    
    private func addAdvertisement() -> UILabel{
        let adView = UILabel()
        
        adView.font = UIFont.systemFont(ofSize: 15.0)
        adView.textAlignment = NSTextAlignment.center
        adView.text = "Обьявление закрыто Google"
        adView.backgroundColor = UIColor.white
        adView.textColor = UIColor.lightGray
        adView.isUserInteractionEnabled = true
        navigationController!.view.addSubview(adView)
        
        adView.translatesAutoresizingMaskIntoConstraints = false
        adView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        navigationController!.view.addConstraints(NSLayoutConstraint.constraints(
                    withVisualFormat: "H:|-0-[ad]-0-|",
                    options: NSLayoutFormatOptions(rawValue: 0), metrics: nil,
                    views: ["ad" : adView]))
        
        return adView
    }
    
    private func addPurchasing() -> UIView{
        let purchasingView = UIView()
        purchasingView.backgroundColor = UIColor.black
        navigationController!.view.addSubview(purchasingView)
        
        purchasingView.translatesAutoresizingMaskIntoConstraints = false
        purchasingView.heightAnchor.constraint(equalToConstant: 45).isActive
                    = true
        navigationController!.view.addConstraints(NSLayoutConstraint.constraints(
                    withVisualFormat: "H:|-0-[purchasing]-0-|",
                    options: NSLayoutFormatOptions(rawValue: 0), metrics: nil,
                    views: ["purchasing" : purchasingView]))
        
        let alert = createAlertMessage()
        let buyButton = createBuyButton()
        
        purchasingView.addSubview(alert)
        purchasingView.addSubview(buyButton)
        
        buyButton.widthAnchor.constraint(equalToConstant: 135).isActive = true
        
        var purchaseDict = Dictionary <String, UIView>()
        purchaseDict["alert"] = alert
        purchaseDict["buyButton"] = buyButton
        
        purchasingView.addConstraints(NSLayoutConstraint.constraints(
                    withVisualFormat: "H:|-0-[alert]-[buyButton]-2-|",
                    options: NSLayoutFormatOptions(rawValue: 0), metrics: nil,
                    views: purchaseDict))
        
        purchasingView.addConstraints(NSLayoutConstraint.constraints(
                    withVisualFormat: "V:|-[alert]-|",
                    options: NSLayoutFormatOptions(rawValue: 0), metrics: nil,
                    views: purchaseDict))
        
        purchasingView.addConstraints(NSLayoutConstraint.constraints(
                    withVisualFormat: "V:|-2-[buyButton]-2-|",
                    options: NSLayoutFormatOptions(rawValue: 0), metrics: nil,
                    views: purchaseDict))
        
        return purchasingView
    }
    
    private func createBuyButton() -> UIButton {
        let buyButton = UIButton()
        
        buyButton.translatesAutoresizingMaskIntoConstraints = false
        buyButton.backgroundColor =
                    UIColor(red: 223/255.0, green: 68/255.0, blue: 68/255.0,
                    alpha: 1.0)
        let lockImage = UIImage(named: "Lock.png")
        buyButton.setImage(lockImage, for: .normal)
        buyButton.setTitle("  СКИДКА -25%", for: .normal)
        buyButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 12.0)
        buyButton.tintColor = UIColor.white
        buyButton.layer.cornerRadius = 2
        buyButton.clipsToBounds = true
        
        return buyButton
    }
    
    private func createAlertMessage() -> UILabel{
        let alert = UILabel()
        
        alert.translatesAutoresizingMaskIntoConstraints = false
        alert.font = UIFont.boldSystemFont(ofSize: 13.0)
        alert.text = "Срок пробной версии истек"
        alert.backgroundColor = UIColor.black
        alert.textColor =
            UIColor(red: 223/255.0, green: 68/255.0, blue: 68/255.0,
                    alpha: 1.0)
        alert.textAlignment = NSTextAlignment.center
        
        return alert
    }
    
    private func addAndroid() -> UIView{
        let androidImage = UIImage.init(named: "Android.png")
        let androidView = UIImageView.init(image: androidImage)
        
        androidView.translatesAutoresizingMaskIntoConstraints = false
        androidView.isUserInteractionEnabled = false
        navigationController!.view.addSubview(androidView)
        
        androidView.widthAnchor.constraint(equalToConstant: 90).isActive = true
        androidView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        androidView.centerXAnchor.constraint(
                    equalTo: navigationController!.view.centerXAnchor).isActive
                    = true
        
        return androidView
    }
    
    private func addMoonButton() -> UIButton{
        let moonButton = UIButton(type: .custom)
        
        moonButton.translatesAutoresizingMaskIntoConstraints = false
        moonButton.setImage(UIImage(named:"CircleButton.png"), for: .normal)
        navigationController!.view.addSubview(moonButton)
        
        moonButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        moonButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        
        return moonButton
    }
    
    private func loadSampleAlarms() {
        guard let alarm1 = Alarm(time: "07:00", periodicity: "ЕЖЕДНЕВНО",
                    awakeChecker: "Арифметика со вводом решения вручную",
                    isEnabled: false) else {
            fatalError("Unable to instantiate alarm1")
        }
        
        guard let alarm2 = Alarm(time: "09:00", periodicity: "СБ ВС",
                    awakeChecker: "0:30 Умное пробуждение", isEnabled: false)
                    else {
            fatalError("Unable to instantiate alarm2")
        }
                        
        alarms = [alarm1, alarm2]
    }
}
