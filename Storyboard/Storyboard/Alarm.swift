//
//  Alarm.swift
//  Storyboard
//
//  Created by alevina on 7/10/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import UIKit

class Alarm {
    
    //MARK: Properties
    var time: String
    var periodicity: String
    var awakeChecker: String?
    var isEnabled: Bool
    
    //MARK: Initialization
    init?(time: String, periodicity: String, awakeChecker: String?,
            isEnabled: Bool) {
        if (time.isEmpty || periodicity.isEmpty) {
            return nil
        }
        
        // Initialize stored properties.
        self.time = time
        self.periodicity = periodicity
        self.awakeChecker = awakeChecker
        self.isEnabled = isEnabled
    }
}
