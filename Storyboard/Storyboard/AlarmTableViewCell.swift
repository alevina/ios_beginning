//
//  AlarmTableViewCell.swift
//  Storyboard
//
//  Created by alevina on 7/10/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import UIKit

class AlarmTableViewCell: UITableViewCell {
    
    //MARK: Properties
    @IBOutlet weak var alarmButton: UIButton!
    @IBOutlet weak var enableAlarmSwitch: UISwitch!
    @IBOutlet weak var optionsButton: UIButton!
    @IBOutlet weak var periodicity: UILabel!
    @IBOutlet weak var awakeChecker: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    @IBAction func enableSwitch(_ sender: UISwitch) {
        
    }
}
