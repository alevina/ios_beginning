//
//  ViewController.swift
//  AddressFinder
//
//  Created by alevina on 6/29/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()
    var mapView: GMSMapView!
    var zoomLevel: Float = 15.0
    
    // A default location. It will be used if location permission is denied.
    let defaultLocation = CLLocation(latitude: 50.27, longitude: 30.31)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func openMapsButton(_ sender: Any) {
        //Location manager initialization
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        
        //Map creation
        createMap(aLatitude: defaultLocation.coordinate.latitude,
                    aLongitude: defaultLocation.coordinate.longitude)
    }
    
    func createMap(aLatitude: CLLocationDegrees,
                   aLongitude: CLLocationDegrees) {
        let camera = GMSCameraPosition.camera(withLatitude: aLatitude,
                    longitude: aLongitude, zoom: 6.0)
        mapView = GMSMapView.map(withFrame: view.bounds, camera: camera)
        
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isMyLocationEnabled = true
        view = mapView
    }
    
    func createMarker(aLatitude: CLLocationDegrees,
                      aLongitude: CLLocationDegrees) {
        mapView.clear()
        
        let marker = GMSMarker()
        
        marker.position = CLLocationCoordinate2D(latitude: aLatitude,
                    longitude: aLongitude)
        marker.title = "Your Position"
        marker.map = mapView
    }
    
//MARK: Delegate
    // Location event callback
    func locationManager(_ manager: CLLocationManager,
                didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        let camera = GMSCameraPosition.camera(withLatitude:
                    location.coordinate.latitude,
                    longitude: location.coordinate.longitude, zoom: zoomLevel)
        mapView.camera = camera
        createMarker(aLatitude: location.coordinate.latitude,
                    aLongitude: location.coordinate.longitude)
    }
    
    // Authorisation status callback
    func locationManager(_ manager: CLLocationManager,
                didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Location manager errors callback
    func locationManager(_ manager: CLLocationManager,
                didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}
