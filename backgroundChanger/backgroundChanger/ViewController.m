//
//  ViewController.m
//  backgroundChanger
//
//  Created by alevina on 6/26/17.
//  Copyright © 2017 alevina. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changeColorOnTouch:(UIButton *)sender
{
    self.view.backgroundColor = [self defineColor];
    self.changeColorButton.backgroundColor = [self defineColor];
}

- (UIColor *)defineColor
{
    return [UIColor colorWithRed:[self generateRandomValue]
                           green:[self generateRandomValue]
                            blue:[self generateRandomValue] alpha:1.0];
}

- (CGFloat)generateRandomValue
{
    CGFloat randomValue = rand() % 255;
    return randomValue / 255.0;
}

@end
