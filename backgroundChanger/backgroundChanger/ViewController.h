//
//  ViewController.h
//  backgroundChanger
//
//  Created by alevina on 6/26/17.
//  Copyright © 2017 alevina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *changeColorButton;

@end
