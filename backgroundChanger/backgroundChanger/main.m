//
//  main.m
//  backgroundChanger
//
//  Created by alevina on 6/26/17.
//  Copyright © 2017 alevina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
