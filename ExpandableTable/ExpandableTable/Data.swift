//
//  File.swift
//  ExpandableTable
//
//  Created by alevina on 7/24/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import Foundation

public class DefaultCellData {
    
    var name: String
    var settings: [ExpansionCellData]?
    
    init(name: String, settings: [ExpansionCellData]?) {
        self.name = name
        self.settings = settings
    }
}

public class ExpansionCellData {
    public var value: String
    
    init(value: String) {
        self.value = value
    }
}
