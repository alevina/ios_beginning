//
//  TableViewController.swift
//  ExpandableTable
//
//  Created by alevina on 7/24/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    var preferences: [DefaultCellData?]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        preferences = loadSettingsData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView,
                numberOfRowsInSection section: Int) -> Int {
        return preferences!.count
    }

    override func tableView(_ tableView: UITableView,
                cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        
        // Row is DefaultCell
        if let rowData = preferences?[indexPath.row] {
            let defaultCell = tableView.dequeueReusableCell(withIdentifier:
                        "DefaultTableViewCell", for: indexPath) as!
                        DefaultTableViewCell
            defaultCell.settingName.text = rowData.name
            cell = defaultCell
        }
        // Row is ExpansionCell
        else {
            //  Get the index of the parent Cell (containing the data)
            let parentCellIndex =
                        getParentCellIndex(expansionIndex: indexPath.row)
            
            let rowData = preferences?[parentCellIndex]
                
            //  Create an ExpansionCell
            let expansionCell = tableView.dequeueReusableCell(withIdentifier:
                        "ExpansionTableViewCell", for: indexPath) as!
                        ExpansionTableViewCell
            
            // Get the index of the expansion cell data
            // (e.g. if there are multiple ExpansionCells
            let expCellIndex = indexPath.row - parentCellIndex - 1
            
            // Set the cell's data
            expansionCell.value.text = rowData?.settings?[expCellIndex].value
            cell = expansionCell
        }
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView,
                didSelectRowAt indexPath: IndexPath) {
        if (nil != (preferences?[indexPath.row])) {
            // If user clicked last cell, do not try to access cell+1
            if(indexPath.row + 1 >= (preferences?.count)!) {
                expandCell(tableView: tableView, index: indexPath.row)
            } else {
                // If next cell is not nil, then cell is not expanded
                if (nil != preferences?[indexPath.row + 1]) {
                    expandCell(tableView: tableView, index: indexPath.row)
                // Close Cell (remove ExpansionCells)
                } else {
                    contractCell(tableView: tableView, index: indexPath.row)
                }
            }
        }
    }
    
    // MARK: Private
    private func loadSettingsData() -> [DefaultCellData?] {
        let languages = [ExpansionCellData(value: "English"),
                    ExpansionCellData(value: "Deutsch"),
                    ExpansionCellData(value: "Français")]
        let languageSettings = DefaultCellData(name: "Language",
                    settings: languages)
        
        let dateFormat = [ExpansionCellData(value: "DD/MM/YYYY"),
                    ExpansionCellData(value: "YYYY/MM/DD")]
        
        let dateSettings = DefaultCellData(name: "Date Format",
                    settings: dateFormat)
        
        return [languageSettings, dateSettings]
    }
    
    private func getParentCellIndex(expansionIndex: Int) -> Int {
        var selectedCell: DefaultCellData?
        var selectedCellIndex = expansionIndex
        
        while(selectedCell == nil && selectedCellIndex >= 0) {
            selectedCellIndex -= 1
            selectedCell = preferences?[selectedCellIndex]
        }
        
        return selectedCellIndex
    }
    
    private func expandCell(tableView: UITableView, index: Int) {
        if let settings = preferences?[index]?.settings {
            for i in 1...settings.count {
                preferences?.insert(nil, at: index + i)
                tableView.insertRows(at: [NSIndexPath(row: index + i,
                            section: 0) as IndexPath] , with: .left)
            }
        }
    }
    
    private func contractCell(tableView: UITableView, index: Int) {
        if let settings = preferences?[index]?.settings {
            for _ in 1...settings.count {
                preferences?.remove(at: index+1)
                tableView.deleteRows(at: [NSIndexPath(row: index + 1,
                            section: 0) as IndexPath], with: .top)
            }
        }
    }
}
