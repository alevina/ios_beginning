//
//  DefaultTableViewCell.swift
//  ExpandableTable
//
//  Created by alevina on 7/23/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import UIKit

class DefaultTableViewCell: UITableViewCell {
    
    //MARK:Properties
    @IBOutlet weak var settingName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
