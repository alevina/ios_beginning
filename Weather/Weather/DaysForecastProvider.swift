//
//  DaysForecastProvider.swift
//  Weather
//
//  Created by alevina on 9/19/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import Foundation

protocol DaysForecastProviderDelegate {
    func didRetrieveDaysForecast(forecast: [[String: AnyObject]])
    func failToRetrieveDaysForecast(error: NSError)}

class DaysForecastProvider {
    private var delegate: DaysForecastProviderDelegate
    
    init(delegate: DaysForecastProviderDelegate) {
        self.delegate = delegate
    }
    
    let amountrOfDays = 3
    
    func getDaysForecast(city: String) {
        let session = URLSession.shared
        
        let daysForecastURL =
                    URL(string: "\(Routes.daysForecastBaseURL)?q=\(city)&" +
                    "\(Keys.units)&cnt=\(amountrOfDays)&" +
                    "APPID=\(Keys.weatherAPIKey)")!
        
        let dataTask = session.dataTask(with: daysForecastURL) { (data,
                    response, error) in
            let httpResponse = response as? HTTPURLResponse
            if let theError = error {
                self.delegate.failToRetrieveDaysForecast(error: theError as
                            NSError)
            } else if 200 != httpResponse?.statusCode {
                let theError = NSError(domain: NSCocoaErrorDomain,
                                       code: NSURLErrorCannotParseResponse,
                                       userInfo: nil)
                self.delegate.failToRetrieveDaysForecast(error: theError)
            } else {
                do {
                    let forecastData =
                                try JSONSerialization.jsonObject(with: data!,
                                options: .mutableContainers) as!
                                [String: AnyObject]
                    
                    let dayListArray =
                                forecastData["list"] as! [[String:  AnyObject]]

                    self.delegate.didRetrieveDaysForecast(forecast: dayListArray)
                } catch let jsonError as NSError {
                    self.delegate.failToRetrieveDaysForecast(error: jsonError)
                }
            }
        }
        
        dataTask.resume()
    }
}
