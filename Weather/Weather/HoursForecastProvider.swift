//
//  HoursForecastProvider.swift
//  Weather
//
//  Created by alevina on 9/9/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import Foundation

protocol HoursForecastProviderDelegate {
    func didRetrieveForecastByHours(forecast: [[String: AnyObject]])
    func failToRetrieveForecastByHours(error: NSError)
}

class HoursForecastProvider {
    private var delegate: HoursForecastProviderDelegate
    
    init(delegate: HoursForecastProviderDelegate) {
        self.delegate = delegate
    }
    
    func retrieveForecastByHours(city: String) {
        let session = URLSession.shared
        let forecastByHoursURL =
                    URL(string: "\(Routes.hoursForecastBaseURL)?q=\(city)&" +
                    "\(Keys.units)&APPID=\(Keys.weatherAPIKey)")!
        
         let dataTask = session.dataTask(with: forecastByHoursURL)
                    { (data, response, error) in
            let httpResponse = response as? HTTPURLResponse
            if let theError = error {
                self.delegate.failToRetrieveForecastByHours(error: theError as
                            NSError)
            } else if 200 != httpResponse?.statusCode {
                let theError = NSError(domain: NSCocoaErrorDomain,
                                       code: NSURLErrorCannotParseResponse,
                                       userInfo: nil)
                self.delegate.failToRetrieveForecastByHours(error: theError)
            } else {
                do {
                    let forecastData =
                                try JSONSerialization.jsonObject(with: data!,
                                options: .mutableContainers) as!
                                [String: AnyObject]
                    
                    let timeListArray = forecastData["list"] as!
                    [[String:  AnyObject]]
                    
                    self.delegate.didRetrieveForecastByHours(forecast:
                                timeListArray)
                } catch let jsonError as NSError {
                    self.delegate.failToRetrieveForecastByHours(error:
                                jsonError as NSError)
                }
            }
        }
        dataTask.resume()
    }
}
