//
//  DayForecastPrototype.swift
//  Weather
//
//  Created by alevina on 9/19/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import UIKit

class DayForecast {
    //MARK: Properties
    var date: String
    var weatherDescription: String
    var weatherIcon: UIImage
    var temperature: String
    
    //MARK: Initialization
    init?(date: String, weatherDescription: String,
          weatherIcon: UIImage,
          temperature: String) {
        self.date = date
        self.weatherDescription = weatherDescription
        self.weatherIcon = weatherIcon
        self.temperature = temperature
    }
}
