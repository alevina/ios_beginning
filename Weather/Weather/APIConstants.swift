//
//  APIConstants.swift
//  Weather
//
//  Created by alevina on 9/9/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import Foundation

struct Routes {
    static let weatherBaseURL = "https://api.openweathermap.org/data/2.5/weather"
    static let weatherIconBaseURL = "https://openweathermap.org/img/w/"
    static let hoursForecastBaseURL = "https://api.openweathermap.org/data/2.5/forecast"
    static let daysForecastBaseURL = "https://api.openweathermap.org/data/2.5/forecast/daily"
}

struct Keys {
    static let weatherAPIKey = "b5961ba0fb94d4eb6d4ca77d3cf59350"
    static let units = "units=metric"
}

enum IconTarget {
    case weather
    case hoursForecast
    case daysForecast
}
