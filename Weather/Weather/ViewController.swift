//
//  ViewController.swift
//  Weather
//
//  Created by alevina on 9/4/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import UIKit

class ViewController: UIViewController, WeatherProviderDelegate,
            UITextFieldDelegate, IconProviderDelegate,
            HoursForecastProviderDelegate, UITableViewDelegate,
            UITableViewDataSource, DaysForecastProviderDelegate
{
    //MARK: Properties
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var getWeatherButton: UIButton!
    @IBOutlet weak var hoursForecastView: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: -
    var weatherProvider: WeatherProvider!
    var iconProvider: IconProvider!
    var hoursForecastProvider: HoursForecastProvider!
    var daysForecastProvider: DaysForecastProvider!
    
    var hoursForecastElements: Array<HoursForecastViewController>!
    var dayForecast: DayForecast!
    var daysForecastElements: Array<DayForecast>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor =
            UIColor(red: 92/255.0, green: 170/255.0, blue: 223/255.0,
                    alpha: 1.0)
        
        weatherProvider = WeatherProvider(delegate: self)
        iconProvider = IconProvider(delegate: self)
        hoursForecastProvider = HoursForecastProvider(delegate: self)
        daysForecastProvider = DaysForecastProvider(delegate: self)
        
        hoursForecastElements = Array<HoursForecastViewController>()
        daysForecastElements = Array<DayForecast>()
        
        cityLabel.text = ""
        descriptionLabel.text = ""
        temperatureLabel.text = ""
        cityTextField.text = ""
        cityTextField.placeholder = "Enter city name"
        cityTextField.delegate = self
        cityTextField.enablesReturnKeyAutomatically = true
        getWeatherButton.isEnabled = false
        
        for _ in 0..<5 {
            let controller = HoursForecastViewController()
            let nibName = "HoursForecastViewElement"
            let containerView = Bundle.main.loadNibNamed(nibName,
                        owner: controller, options: nil)?.last
            hoursForecastElements.append(controller)
            hoursForecastView.addArrangedSubview(containerView as! UIStackView)
        }
        
        self.loadSampleDaysForecast()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Actions
    @IBAction func getWeatherButtonTapped(_ sender: UIButton) {
        guard let text = cityTextField.text, !text.isEmpty else {
            return
        }
        
        let cityName = cityTextField.text!.urlEncoded
        
        weatherProvider.getWeather(city: cityName)
        hoursForecastProvider.retrieveForecastByHours(city: cityName)
        daysForecastProvider.getDaysForecast(city: cityName)
    }
    
    //MARK: WeatherProviderDelegateMethods
    func didRetrieveWeather(weather: Weather) {
        DispatchQueue.main.async() {
            self.cityLabel.text = weather.city
            self.descriptionLabel.text = weather.weatherDescription
            self.temperatureLabel.text = "\(Int(round(weather.temp)))°"
            let weatherIconId = weather.weatherIconId
            self.iconProvider.getWeatherIcon(iconId: weatherIconId,
                        target: .weather, index: nil)
        }
    }
    
    func failToRetrieveWeather(error: NSError) {
        DispatchQueue.main.async() {
            self.showAlert(title: "Cannot retrieve weather",
                        message: "The weather service is not responding")
        }
        print("Fail to retrieve weather with error: \(error)")
    }
    
    //MARK: HoursForecastProviderDelegateMethods
    func didRetrieveForecastByHours(forecast: [[String: AnyObject]]) {
        DispatchQueue.main.async() {
            var elementIndex = 0
            for element in self.hoursForecastElements {
                let hoursForecastData =
                            Forecast(forecastData: forecast[elementIndex])
                element.dateLabel.text = hoursForecastData.date
                element.timeLabel.text = hoursForecastData.time
                let hoursForecastIconId = hoursForecastData.weatherIconId
                self.iconProvider.getWeatherIcon(iconId: hoursForecastIconId, target: .hoursForecast, index: elementIndex)
                element.temperatureLabel.text = "\(Int(round(hoursForecastData.hoursTemperature!)))°"
                elementIndex += 1
            }
        }
    }
    
    func failToRetrieveForecastByHours(error: NSError) {
        DispatchQueue.main.async() {
            self.showAlert(title: "Cannot retrieve weather forecast",
                        message: "The weather forecast service is not " +
                        "responding")
        }
        print("Fail to retrieve weather forecast with error: \(error)")
    }
    
    //MARK: DaysForecastProviderDelegateMethods
    func didRetrieveDaysForecast(forecast: [[String : AnyObject]]) {
        DispatchQueue.main.async() {
            for element in 0..<forecast.count {
                let daysForecastData = Forecast(forecastData: forecast[element])
                self.daysForecastElements[element].date = daysForecastData.date
                self.daysForecastElements[element].temperature =
                            "\(Int(round(daysForecastData.daysTemperature!)))°"
                self.daysForecastElements[element].weatherDescription =
                            daysForecastData.weatherDescription
                let daysForecastIconId = daysForecastData.weatherIconId
                self.iconProvider.getWeatherIcon(iconId: daysForecastIconId,
                            target: .daysForecast, index: element)
            }
        }
    }
    
    func failToRetrieveDaysForecast(error: NSError) {
        DispatchQueue.main.async() {
            self.showAlert(title: "Cannot retrieve weather forecast",
                        message: "The weather forecast service is not" +
                        "responding")
        }
        print("Fail to retrieve weather forecast with error: \(error)")
    }
    
    //MARK: IconProviderDelegateMethods
    func didRetrieveIcon(data: Data, target: IconTarget, index: Int?) {
        DispatchQueue.main.async() {
            let weatherImage = UIImage(data: data)
            switch target {
            case .weather:
                self.weatherIcon.image = weatherImage
            case .hoursForecast:
                self.hoursForecastElements[index!].icon.image = weatherImage
            case .daysForecast:
                self.daysForecastElements[index!].weatherIcon = weatherImage!
                if (self.daysForecastProvider.amountrOfDays-1 == index) {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func failToRetrieveIcon(error: NSError) {
        print("Fail to retrieve weather icon with error: \(error)")
    }
    
    //MARK: UITextFieldDelegate
    func textField(_ textField: UITextField,
                shouldChangeCharactersIn range: NSRange,
                replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        let prospectiveText =
                    (currentText as NSString).replacingCharacters(in: range,
                    with: string)
        getWeatherButton.isEnabled = prospectiveText.characters.count > 0
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        getWeatherButton.isEnabled = false
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        getWeatherButtonTapped(getWeatherButton)
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    //MARK: UITableViewDataSource methods
    public func tableView(_ tableView: UITableView,
                numberOfRowsInSection section: Int) -> Int {
        return daysForecastElements.count
    }

    public func tableView(_ tableView: UITableView,
                cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "day",
                    for: indexPath) as? DayForecastCell
        
        let dayForecast = daysForecastElements[indexPath.row]
        
        cell?.day.text = dayForecast.date
        cell?.weatherDescription.text = dayForecast.weatherDescription
        cell?.icon.image = dayForecast.weatherIcon
        cell?.dayTemperature.text = dayForecast.temperature
        
        return cell!;
    }
    
    //MARK: Private functions
    private func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message,
                    preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok",
                    style: UIAlertActionStyle.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    private func loadSampleDaysForecast(){
        for _ in 0..<daysForecastProvider.amountrOfDays {
            let image = UIImage(named: "cap.png")
            let dayForecast = DayForecast(date: "", weatherDescription: "",
                        weatherIcon: image!, temperature: "")
            daysForecastElements.append(dayForecast!)
        }
    }
}

extension String {
    var urlEncoded: String {
        return self.addingPercentEncoding(withAllowedCharacters:
                    CharacterSet.urlUserAllowed)!
    }
}
