//
//  Weather.swift
//  Weather
//
//  Created by alevina on 9/4/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import Foundation

struct Weather {
    let city: String
    
    let weatherDescription: String
    let weatherIconId: String
    
    let temp: Double
    
    init(weatherData: [String: AnyObject]) {
        city = weatherData["name"] as! String
        
        let weatherDict = weatherData["weather"]![0]! as! [String: AnyObject]
        weatherDescription = weatherDict["description"] as! String
        weatherIconId = weatherDict["icon"] as! String
        
        let mainDict = weatherData["main"] as! [String: AnyObject]
        temp = mainDict["temp"] as! Double
    }
}
