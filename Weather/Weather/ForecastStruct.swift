//
//  HoursForecastStruct.swift
//  Weather
//
//  Created by alevina on 9/11/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import Foundation

struct Forecast {
    let date: String
    let time: String
    let weatherIconId: String
    let weatherDescription: String
    let hoursTemperature: Double?
    let daysTemperature: Double?
    
    init(forecastData: [String: AnyObject]) {
        
        let dateAndTime = NSDate(timeIntervalSince1970: forecastData["dt"] as!
                    TimeInterval)
        
        date = dateAndTime.getDate(dateAndTime)
        time = dateAndTime.getTime(dateAndTime)
        
        let weatherDict = forecastData["weather"]![0]! as! [String: AnyObject]
        weatherIconId = weatherDict["icon"] as! String
        weatherDescription = weatherDict["description"] as! String
        
        if (forecastData["main"] != nil) {
            let mainDict = forecastData["main"] as! [String: AnyObject]
            hoursTemperature = mainDict["temp"] as? Double
        } else {
            hoursTemperature = nil
        }
        if (forecastData["temp"] != nil) {
            let tempDict = forecastData["temp"] as! [String: AnyObject]
            daysTemperature = tempDict["day"] as? Double
        } else {
            daysTemperature = nil
        }
    }
}

extension NSDate {
    func getTime(_ dateAndTime: NSDate) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: dateAndTime as Date)
    }
    
    func getDate(_ dateAndTime: NSDate) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM"
        return dateFormatter.string(from: dateAndTime as Date)
    }
}
