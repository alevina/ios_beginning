//
//  HoursForecastViewController.swift
//  Weather
//
//  Created by alevina on 9/14/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import UIKit

class HoursForecastViewController: NSObject {
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
}
