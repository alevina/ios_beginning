//
//  DayForecastCell.swift
//  Weather
//
//  Created by alevina on 9/19/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import UIKit

class DayForecastCell: UITableViewCell {
    
    //MARK: Properties
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    @IBOutlet weak var dayTemperature: UILabel!
}
