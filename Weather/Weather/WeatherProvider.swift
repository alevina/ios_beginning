//
//  WeatherProvider.swift
//  Weather
//
//  Created by alevina on 9/4/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import Foundation

protocol WeatherProviderDelegate {
    func didRetrieveWeather(weather: Weather)
    func failToRetrieveWeather(error: NSError)
}

class WeatherProvider {
    private var delegate: WeatherProviderDelegate
    
    init(delegate: WeatherProviderDelegate) {
        self.delegate = delegate
    }
    
    func getWeather(city: String) {
        let session = URLSession.shared
        let weatherRequestURL =
                    URL(string: "\(Routes.weatherBaseURL)?q=\(city)&" +
                    "\(Keys.units)&APPID=\(Keys.weatherAPIKey)")!
        
        let dataTask = session.dataTask(with: weatherRequestURL) { (data,
                    response, error) in
            let httpResponse = response as? HTTPURLResponse
            if let theError = error {
                self.delegate.failToRetrieveWeather(error: theError as NSError)
            } else if 200 != httpResponse?.statusCode {
                let theError = NSError(domain: NSCocoaErrorDomain,
                            code: NSURLErrorCannotParseResponse,
                            userInfo: nil)
                self.delegate.failToRetrieveWeather(error: theError)
            } else {
                do {
                    let weatherData =
                                try JSONSerialization.jsonObject(with: data!,
                                options: .mutableContainers) as!
                                [String: AnyObject]
                    let weather = Weather(weatherData: weatherData)
                    self.delegate.didRetrieveWeather(weather: weather)
                } catch let jsonError as NSError {
                    self.delegate.failToRetrieveWeather(error: jsonError)
                }
            }
        }
        
        dataTask.resume()
    }
}
