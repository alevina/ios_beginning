//
//  IconProvider.swift
//  Weather
//
//  Created by alevina on 9/7/17.
//  Copyright © 2017 alevina. All rights reserved.
//

import Foundation

protocol IconProviderDelegate {
    func didRetrieveIcon(data: Data, target: IconTarget, index: Int?)
    func failToRetrieveIcon(error: NSError)
}

class IconProvider {  
    private var delegate: IconProviderDelegate
    
    init(delegate: IconProviderDelegate){
        self.delegate = delegate
    }
    
    func getWeatherIcon(iconId: String, target: IconTarget, index: Int?) {
        let session = URLSession.shared
        let weatherIconRequestURL = URL(string: "\(Routes.weatherIconBaseURL)" +
                    "\(iconId).png")!
        let dataTask = session.dataTask(with: weatherIconRequestURL) { (data,
                    response, error) in
            let httpResponse = response as? HTTPURLResponse
            if let theError = error {
                self.delegate.failToRetrieveIcon(error: theError as NSError)
            } else if 200 != httpResponse?.statusCode {
                let theError = NSError(domain: NSCocoaErrorDomain,
                                       code: NSURLErrorCannotParseResponse,
                                       userInfo: nil)
                self.delegate.failToRetrieveIcon(error: theError)
            } else {
                self.delegate.didRetrieveIcon(data: data!, target: target,
                            index: index)
            }
        }
        
        dataTask.resume()
    }
}
